# Exploracion_Visualizacion_Datos

Práctica del módulo de Exploración y visualización de datos del BootCamp Full Stack Big Data AI y ML VI

# Mi objetivo, es mostrar la evolución de los precios mensuales, en la comunidad de Madrid, desde 2010 hasta 2016.

En primer lugar, he filtrado los datos en la entrada de datos, los campo que no he usado los he ocultado, he hechomodificaciones como la de la latitud, altitud, filtrado de años y he eliminado nulos.

Mi KPI creada ha sido Precio_fianza_limpieza, campo calculado en el que sumo los precios, fianza y limpieza, eliminando nulos. 

El gráfico prom_anual_mes, muestra la evolución de los precios por mese y alos en la comunidad de Madrid, en la descripción emergente muestro infromación mas detallada.

El gráfico barrios, muestra la información que indquemos en el TOP, por ejemplo, el top 5 de Madrid que es el minimo a mostrar, nos idicará en el mapa donde están ubicados, y segun su color indicará el precio mas bajo a mas alto, en la descripción emergente, muestro información mas detallada. 
Queria hacerlo por zonas, (gráfico zonas), y mostrara el TOP por zona Norte, Sur y Centro pero no he conseguido que me salga, creo que es por el tema de los códigos postales.

El gráfico cancellation_Policy, es un gráfico de anillo que utilizo en la descripción emergente del mapa de barrios.

El gráfico Top nos mostrara el top de Madrid por política de calcelación y en l descrición emergente nos indicara ademas, el promedio del número de reviews.

El gráfico prom_anual_zonas nos indica el prom del precio en Madrid por año y zona.

El dasboard Resultado, muestro el resultado final de mi objetivo, por defecto tenemos el top 5 de la comunidad de Madrid, que se mostrara los barrios en el mapa y podremos consultar los detalles.

Es un gráfico sencillo, y muy concreto. 






